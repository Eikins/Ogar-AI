#include "controller_gradient.h"

#define RADIUS_FACTOR 2

#define SECURITY_OFFSET 0.01
#define MINUS_INF -10000000
#define MOVE_FACTOR 5

#define SPLIT_DIST 4

#define VIRUS_GAIN_FACTOR 0.05
#define FOOD_GAIN_FACTOR 1
#define PLAYER_GAIN_FACTOR 0.7
#define DANGER_GAIN_LOSS_FACTOR 15

#define SQUEEZE_REDUCTION 0.1

#define SPLIT_COOLDOWN 25

int splitCdCount = 0;

unsigned int lastId = 0;

void __ControllerGradient__update(Controller* controller) {

  if(splitCdCount > 0) splitCdCount--;

  if(playerCells == NULL) return;

  Node* playerNode = NULL;
  double maxGain = MINUS_INF;
  Node* targetNode = NULL;
  // Resetting all gain for current iteration.
  for(playerNode = *cells; playerNode != NULL; playerNode = playerNode ->next) {
    playerNode->gain = 0;
  }

  // For each player nodes...
  for(playerNode = *playerCells; playerNode != NULL; playerNode = playerNode->next) {

    Node* node;
    // For each nodes...
    for(node = *cells; node != NULL; node = node->next) {
      // If this cell is not own by the client...
      if(getNode(playerCells, node->id) == NULL) {
        calculateGain(playerNode, node);
      }
    }

    for(node = *cells; node != NULL; node = node->next) {
      if(getNode(playerCells, node->id) == NULL) {
        double distance = dist(node, playerNode);
        if(node->gain / distance > maxGain) {
          targetNode = node;
          maxGain = node->gain / distance;
        }
      }
    }
  }



  if(targetNode != NULL) {
    if(lastId != 0) {
      Node* lastNode = getNode(cells, lastId);
      if(lastNode != NULL && fabs(targetNode->gain - lastNode->gain) < SQUEEZE_REDUCTION) {
        targetNode = lastNode;
      }
    }

    controller->targetX = centerPosX + ((targetNode->x - centerPosX) * MOVE_FACTOR);
    controller->targetY = centerPosY + ((targetNode->y - centerPosY) * MOVE_FACTOR);
    if(playerCells != NULL && *playerCells != NULL) {
      if(splitCdCount <= 0 && isWorthSplitting(targetNode)) {
        controller->split = 1;
        splitCdCount = SPLIT_COOLDOWN;
      }
    }
    lastId = targetNode->id;
  }

}

void calculateGain(Node* playerNode, Node* node) {
  Node* nodeAround;
  // For each cell...
  for(nodeAround = *cells; nodeAround != NULL; nodeAround = nodeAround->next) {
    double distance = dist(nodeAround, node);
    if(distance == 0)  continue;
    if(canEatSecurly(playerNode, node)) {
        if((node->flags & 8) == 8) {
          nodeAround->gain += node->size * PLAYER_GAIN_FACTOR / distance;
        } else if((node->flags & 1) == 1) {
          nodeAround->gain += node->size * VIRUS_GAIN_FACTOR / distance;
        } else {
          nodeAround->gain += node->size * FOOD_GAIN_FACTOR / distance;
        }
    } else if((node->flags & 8) == 8) {
        nodeAround->gain -= node->size * DANGER_GAIN_LOSS_FACTOR / distance;
    }
  }
  if(canEatSecurly(playerNode, node)) {
    if((node->flags & 8) == 8) {
      node->gain += node->size * PLAYER_GAIN_FACTOR;
    } else if((node->flags & 1) == 1) {
      node->gain += node->size * VIRUS_GAIN_FACTOR;
    } else {
      node->gain += node->size * FOOD_GAIN_FACTOR;
    }
  } else if((node->flags & 8) == 8) {
      node->gain -= node->size * DANGER_GAIN_LOSS_FACTOR;
  }
}

int canEatSecurly(Node* playerNode, Node* node) {
  return playerNode->size >= (1.1402 + SECURITY_OFFSET) * node->size;
}

double dist(Node* n1, Node* n2) {
  return sqrt((n1->x - n2->x) * (n1->x - n2->x) + (n1->y - n2->y) * (n1->y - n2->y));
}

int isWorthSplitting(Node* target) {
  if((target->flags & 8) == 0 || getNode(playerCells, target->id) != NULL) return 0;
  Node* playerNode = NULL;
  for(playerNode = *playerCells; playerNode != NULL; playerNode = playerNode->next) {
    if((playerNode->size / 2 >= (1.1402 + SECURITY_OFFSET) * target->size) && dist(playerNode, target) <= SPLIT_DIST * playerNode->size) {
      return 1;
    }
  }
  return 0;
}
