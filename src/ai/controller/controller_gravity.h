/**
 * \file controller_gradient.h
 * \brief Implémentation du contrôleur AI-Gradient.
 * \author Noé.M
 * \version 0.1
 * \date 12 Juin 2018
 *
 * Implémentation du contrôleur AI-Gradient.
 *
 */

/**
 * \fn void __ControllerGradient__update(Controller* controller)
 * \brief Implémentation du controller AI-Gradient
 *
 * \param Controller* controller
 */

void __ControllerGravity__update(Controller* controller);
