/**
 * \file controller_manual.h
 * \brief Implémentation du contrôleur Manual.
 * \author Noé.M
 * \version 0.1
 * \date 05 Juin 2018
 *
 * Implémentation du contrôleur Manual.
 *
 */

/**
 * \fn void __ControllerManual__update(Controller* controller)
 * \brief Implémentation du controller Manual
 *
 * \param Controller* controller
 */
void __ControllerManual__update(Controller* controller);
