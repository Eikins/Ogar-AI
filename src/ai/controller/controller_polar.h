#define ANGLE_PRECISION 64

#define MIN_INF -1000000000

#define EAT_SECURITY_OFFSET 0.1

#define VIRUS_GAIN 10
#define FOOD_GAIN 0.5
#define PLAYER_GAIN 0.7
#define DANGER_GAIN_LOSS 20
#define BORDER_GAIN_LOSS 50

void __ControllerPolar__update(Controller* controller);


//AI COMPONENTS

typedef struct {
  int x, y;
} Vec2;

void bestDirection(Node* playerNode, float* gain, Vec2* targetDirection);

void calcGainMap(Node* playerNode);

int canEat(Node* playerNode, Node* node) ;

// PHYSICS
Vec2 Vector2(int x, int y);

Vec2 FromPolar(float magnitude, float angle);

float magnitude(Vec2 vec);

float dot(Vec2 v1, Vec2 v2);

float angle(Vec2 vec);

float angleBetween(Vec2 v1, Vec2 v2);

Vec2 normalized(Vec2 vec);

Vec2 scale(Vec2 vec, float scale);

double nodeDistance(Node* n1, Node* n2);
