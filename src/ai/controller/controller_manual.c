#include "controller_manual.h"

void __ControllerManual__update(Controller* controller) {
	SDL_Event event;                                               
	while( SDL_PollEvent( &event ) ){
		switch( event.type ){
		case SDL_KEYDOWN:
                switch( event.key.keysym.sym ){
                    case SDLK_SPACE:
                        controller->split = 1;
                        break;
                    case SDLK_w:
                        controller->ejectMass = 1;
                        break;
                    default:
                        break;
                }
			break;
		default:
			break;
		}
	}
	int mouseX, mouseY;
	SDL_PumpEvents();
	SDL_GetMouseState(&mouseX, &mouseY);

	if(sightRangeX == 0) {
		controller->targetX = (centerPosX - screenWidth / 2 + mouseX);
		controller->targetY = (centerPosY - screenHeight / 2 + mouseY);
	} else {
		controller->targetX = centerPosX + (- screenWidth / 2 + mouseX) * screenWidth / sightRangeX;
		controller->targetY = centerPosY + (- screenHeight / 2 + mouseY) * screenHeight / sightRangeY;
	}
	
}

