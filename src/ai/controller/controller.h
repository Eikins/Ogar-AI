/**
 * \file controller.h
 * \brief Module de gestion des contrôles.
 * \author Noé.M
 * \version 0.1
 * \date 05 Juin 2018
 *
 * Module de gestion des contrôles.
 *
 */

#define CONTROLLER_MANUAL 0
#define CONTROLLER_LAZY 1
#define CONTROLLER_RAY 2
#define CONTROLLER_GRADIENT 3
#define CONTROLLER_GRAVITY 4
#define CONTROLLER_POLAR 5

/**
 * \struct Controller
 * \brief Objet contrôleur qui permet de jouer au jeu.
 *
 * Utilisé pour les entrées, ainsi, le joueur et les IA implémentent cet objet.
 *
 */
typedef struct Controller {
	int targetX, targetY;
	int split;
	int ejectMass;
	void (*update) (struct Controller* controller);
} Controller;

/**
 * \fn Controller* buildController(int controller, ...)
 * \brief Constructeur de l'objet controller.
 *
 * \param controller Identifiant du controller, voir la liste des controllers.
 * \return Controller
 */
Controller* buildController(int controller, ...);


extern Controller* controller;
