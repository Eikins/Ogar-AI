#include "controller_lazy.h"

void __ControllerLazy__update(Controller* controller) {
	
	double minSquareDist = 10000000;
	if(cells != NULL && *cells != NULL) {
		Node* node = *cells;
		while(node != NULL) {
			if((node->flags & 8) != 8 && (node->flags & 1) != 1) {
				double squareDist = (node->x - centerPosX) * (node->x - centerPosX) + (node->y - centerPosY) * (node->y - centerPosY);
				if(squareDist <= minSquareDist) {
					controller->targetX = node->x;
					controller->targetY = node->y;
					minSquareDist = squareDist;
				}
			}
			node = node->next;
		}
	}	
}

