#include "controller_polar.h"

void __ControllerPolar__update(Controller* controller) {

  Node* playerNode;
  Vec2 targetDirection = Vector2(0, 0);
  float targetDirectionGain = MIN_INF;
  // For each player cell...
  for(playerNode = *playerCells; playerNode != NULL; playerNode = playerNode->next) {
    // First, calculate the gain Map
    calcGainMap(playerNode);
    // Then, calculate cones total gain, and choose direction based on gain
    bestDirection(playerNode, &targetDirectionGain, &targetDirection);
  }
  // Setting controller target
  //targetDirection = scale(targetDirection, 2);
  controller->targetX = centerPosX + targetDirection.x;
  controller->targetY = centerPosY + targetDirection.y;

}

void bestDirection(Node* playerNode, float* gain, Vec2* targetDirection) {
  // For each angle between 0 and 360 with a step of ANGLE_PRECISION
  int i;
  for(i = 0; i < ANGLE_PRECISION; i++) {
    Node* node;
    float totalConeGain = 0;
    Vec2 direction = FromPolar(1000, ((float) i * 2 * M_PI / ANGLE_PRECISION));
    for(node = *cells; node != NULL; node = node->next) {
      if(getNode(playerCells, node->id) == NULL) {
        Vec2 nodeRelativeDirection = Vector2(node->x - playerNode->x, node->y - playerNode->y);
        if(abs(angleBetween(direction, nodeRelativeDirection)) <= (M_PI / ANGLE_PRECISION)) {
          // Here we have the cells in the cone
          double distance = nodeDistance(playerNode, node);
          if(distance < 1) distance = 1;
          totalConeGain += node->gain / distance / distance;
        }
      }
    }

    // Border distance
    // Top
    Vec2 toBorderLeft = Vector2(viewField.left - playerNode->x, 0);
    Vec2 toBorderRight = Vector2(viewField.right - playerNode->x, 0);
    Vec2 toBorderTop = Vector2(0, viewField.top - playerNode->y);
    Vec2 toBorderBottom = Vector2(0, viewField.bottom - playerNode->y);

    if(magnitude(toBorderLeft) == 0) {
      toBorderLeft = Vector2(-1, 0);
    }
    totalConeGain -= dot(normalized(toBorderLeft), normalized(direction)) * BORDER_GAIN_LOSS / magnitude(toBorderLeft) / magnitude(toBorderLeft);

    if(magnitude(toBorderRight) == 0) {
      toBorderLeft = Vector2(1, 0);
    }
    totalConeGain -= dot(normalized(toBorderRight), normalized(direction)) * BORDER_GAIN_LOSS / magnitude(toBorderRight) / magnitude(toBorderLeft);

    if(magnitude(toBorderTop) == 0) {
      toBorderLeft = Vector2(0, -1);
    }
    totalConeGain -= dot(normalized(toBorderTop), normalized(direction)) * BORDER_GAIN_LOSS / magnitude(toBorderTop) / magnitude(toBorderLeft);

    if(magnitude(toBorderBottom) == 0) {
      toBorderLeft = Vector2(0, 1);
    }
    totalConeGain -= dot(normalized(toBorderBottom), normalized(direction)) * BORDER_GAIN_LOSS / magnitude(toBorderBottom) / magnitude(toBorderLeft);

    if(totalConeGain > *gain) {
      targetDirection->x = direction.x + playerNode->x - centerPosX;
      targetDirection->y = direction.y + playerNode->y - centerPosY;
      *gain = totalConeGain;
    }
  }
}

void calcGainMap(Node* playerNode) {
  Node* node;
  for(node = *cells; node != NULL; node = node->next) {
    node->gain = 0;
  }
  // For each nodes...
  for(node = *cells; node != NULL; node = node->next) {
    // If this cell is not own by the client...
    if(getNode(playerCells, node->id) == NULL) {

      Node* nodeAround;
      // For each cell...
      for(nodeAround = *cells; nodeAround != NULL; nodeAround = nodeAround->next) {
        double distance = nodeDistance(nodeAround, node);
        if(distance < 1) distance = 1;
        if(canEat(playerNode, node)) {
            if((node->flags & 8) == 8) {
              nodeAround->gain += node->size * PLAYER_GAIN / distance;
            } else if((node->flags & 1) == 1) {
              nodeAround->gain -= node->size * VIRUS_GAIN / distance;
            } else {
              nodeAround->gain += node->size * FOOD_GAIN / distance;
            }
        } else if((node->flags & 8) == 8) {
            nodeAround->gain -= node->size * DANGER_GAIN_LOSS / distance;
        } else if((node->flags & 1) == 1) {
            nodeAround->gain -= node->size * VIRUS_GAIN / distance;
        }
      }
    }
  }
}

int canEat(Node* playerNode, Node* node) {
  return playerNode->size >= (1.1402 + EAT_SECURITY_OFFSET) * node->size;
}

Vec2 Vector2(int x, int y) {
  Vec2 vec;
  vec.x = x;
  vec.y = y;
  return vec;
}

Vec2 FromPolar(float magnitude, float angle) {
  Vec2 vec;
  vec.x = (int) (magnitude * cosf(angle));
  vec.y = (int) (magnitude * sinf(angle));
  return vec;
}

float magnitude(Vec2 vec) {
  return sqrt(vec.x * vec.x + vec.y * vec.y);
}

float dot(Vec2 v1, Vec2 v2) {
  return v1.x * v2.x + v1.y * v2.y;
}

float angle(Vec2 vec) {
  return angleBetween(vec, Vector2(1, 0));
}

float angleBetween(Vec2 v1, Vec2 v2) {
  return acosf(dot(v1, v2) / (magnitude(v1) * magnitude(v2)));
}

Vec2 normalized(Vec2 vec) {
  float mag = magnitude(vec);
  if(mag != 0) {
    vec.x = vec.x / mag;
    vec.y = vec.y / mag;
  }
  return vec;
}

Vec2 scale(Vec2 vec, float scale) {
  vec.x *= scale;
  vec.y *= scale;
  return vec;
}

double nodeDistance(Node* n1, Node* n2) {
  return sqrt((n1->x - n2->x) * (n1->x - n2->x) + (n1->y - n2->y) * (n1->y - n2->y)) - n2->size - n1->size;
}
