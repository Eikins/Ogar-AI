#include "controller.h"
#include "controller_manual.c"
#include "controller_lazy.c"
#include "controller_ray.c"
#include "controller_gradient.c"
#include "controller_gravity.c"
#include "controller_polar.c"

Controller* controller;

Controller* buildController(int controller, ...) {
	Controller* ret = malloc(sizeof(Controller));
	ret->split = 0;
	ret->ejectMass = 0;
	ret->targetX = 0;
	ret->targetY = 0;
	switch(controller) {

	case CONTROLLER_MANUAL:
		ret->update = __ControllerManual__update;
		break;
	case CONTROLLER_LAZY:
		ret->update = __ControllerLazy__update;
		break;
	case CONTROLLER_RAY:
		ret->update = __ControllerRay__update;
		break;
	case CONTROLLER_GRADIENT:
		ret->update = __ControllerGradient__update;
		break;
	case CONTROLLER_GRAVITY:
		ret->update = __ControllerGravity__update;
		break;
	case CONTROLLER_POLAR:
		ret->update = __ControllerPolar__update;
		break;
	default:
		ret->update = __ControllerManual__update;
		break;
	}
	return ret;
}
