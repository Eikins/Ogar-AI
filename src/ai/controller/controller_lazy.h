/**
 * \file controller_lazy.h
 * \brief Implémentation du contrôleur AI-Lazy.
 * \author Noé.M
 * \version 0.1
 * \date 05 Juin 2018
 *
 * Implémentation du contrôleur AI-Lazy.
 *
 */

/**
 * \fn void __ControllerLazy__update(Controller* controller)
 * \brief Implémentation du controller AI-Lazy
 *
 * \param Controller* controller
 */
void __ControllerLazy__update(Controller* controller);
