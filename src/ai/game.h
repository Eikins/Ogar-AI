#include "libwebsockets.h"

/**
 * \file game.h
 * \brief Module principal du jeu.
 * \author Noé.M
 * \version 0.1
 * \date 01 Juin 2018
 *
 * Module principal du jeu Ogar.
 *
 */

/**
 * \struct ViewField
 * \brief Représentation de la vue du joueur.
 */
typedef struct ViewField {
	double left;
	double top;
	double right;
	double bottom;
} ViewField;

/**
 * \struct Node
 * \brief Element cellule d'une liste chainée.
 *
 */
typedef struct Node {
	double gain;
	unsigned int id, size;
	int x, y;
	unsigned char r, g, b, flags;
	char* name;
	struct Node* next;
} Node;

extern Node** cells;

extern Node** playerCells;
extern int playerCellIds[16];

extern int centerPosX;
extern int centerPosY;
extern int sightRangeX;
extern int sightRangeY;

extern ViewField viewField;

/**
 * \fn void update (struct lws *wsi)
 * \brief Boucle principale du client.
 * On l'utilisera pour la boucle principale du client.
 *
 */
void update(struct lws *wsi);

void renderDisplay();

void calcCenterAndSight();

void addNode(Node** list, Node* element);

void removeNode(Node** list, unsigned int id);

Node* getNode(Node**list, unsigned int id);

void clearNodeList(Node** list);

void foreachNode(Node** list, void (*func)(Node* node));
