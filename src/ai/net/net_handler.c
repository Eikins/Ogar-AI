#include <stdlib.h>

#include "packets.c"
#include "net_handler.h"
#include "../game.c"

void handleMessage(struct lws *wsi, unsigned char* bytes, int length) {
	if(length <= 0) {
		logs->warn(logs, "NetHandler", "No data received, skipping parsing...");
	}
	unsigned int packetId = (unsigned int) bytes[0];

	switch(packetId) {

	case SPACKET_ADD_NODE:
		logs->info(logs, "NetHandler", "Add_Node packet received (opcode 32).");
		if(length >= 5) {
			struct s_AddNode* packet = (struct s_AddNode*) (bytes+1);
			handleAddNodePacket(wsi, *packet);
		} else {
			// Should never happen as we use TCP but, nevermind.
			logs->err(logs, "NetHandler", "Add_Node packet corrupted, skipping.");
		}
		break;
	case SPACKET_CLEAR_NODES:
		logs->info(logs, "NetHandler", "Clear_Nodes packet received. (opcode 18)");
		handleClearNodesPacket(wsi);
		break;
	case SPACKET_DRAW_LINE:
		logs->info(logs, "NetHandler", "Draw_Line packet received. (opcode 21)");
		if(length >= 5) {
			struct s_DrawLine* packet = (struct s_DrawLine*) (bytes+1);
			handleDrawLinePacket(wsi, *packet);
		} else {
			// Should never happen as we use TCP but, nevermind.
			logs->err(logs, "NetHandler", "Draw_Line packet corrupted, skipping.");
		}
		break;
	case SPACKET_SET_BORDERS:
		logs_Updates->info(logs_Updates, "NetHandler", "Set_Borders packet received. (opcode 64)");
		if(length >= 33) {
			struct s_SetBorders* packet = (struct s_SetBorders*) (bytes+1);
			handleSetBordersPacket(wsi, *packet);
		} else {
			// Should never happen as we use TCP but, nevermind.
		//	logs->err(logs, "NetHandler", "Set_Borders packet corrupted, skipping.");
		}
		break;
	case SPACKET_UPDATE_NODES:
		logs_Updates->info(logs_Updates, "NetHandler", "Update_Nodes packet received. (opcode 16)");

	// Parsing UpdateNodes packet...

    struct s_UpdateNodes packet;
    packet.deSize = (short unsigned int) bytes[1] + (bytes[2] << 8);
    packet.deadNodes = malloc(sizeof(struct s16_DeadNodes) * packet.deSize);
    int i;
    for(i = 0; i < packet.deSize; i++) {
        packet.deadNodes[i].eaterId = readUIntFromBuffer(bytes, 3 + i * 8, 4);
        packet.deadNodes[i].eatenId = readUIntFromBuffer(bytes, 7 + i * 8, 4);

        logs_Updates->info(logs_Updates, "NetHandler", "Dead Node : %d %d", packet.deadNodes[i].eaterId, packet.deadNodes[i].eatenId);
    }

    int offset = 3 + packet.deSize * 8;

		packet.nodes = NULL;

    if(readUIntFromBuffer(bytes, offset, 4) != 0) {

      packet.nodes = malloc(sizeof(struct s16_Node));
      struct s16_Node* node;
      node = packet.nodes;
      node->id = readUIntFromBuffer(bytes, offset, 4); offset += 4;
      node->x = readIntFromBuffer(bytes, offset); offset += 4;
      node->y = readIntFromBuffer(bytes, offset); offset += 4;
      node->size = readUIntFromBuffer(bytes, offset, 2); offset += 2;
      node->flags = bytes[offset++];
      node->R = bytes[offset++];
      node->G = bytes[offset++];
      node->B = bytes[offset++];
      if((node->flags & 8) == 8) {
        int slen = strlen((char*) (bytes + offset)) + 1;
        node->name = malloc(sizeof(char) * slen);
        strcpy(node->name, (char*) (bytes + offset));
        offset += slen;
        logs_Updates->info(logs_Updates, "NetHandler", "Node : %d %d %d %d %d %d %d %d %s", node->id, node->x, node->y, node->size, node->flags, node->R, node->G, node->B, node->name);
      } else {
        logs_Updates->info(logs_Updates, "NetHandler", "Node : %d %d %d %d %d %d %d %d", node->id, node->x, node->y, node->size, node->flags, node->R, node->G, node->B);
      }

      while (readUIntFromBuffer(bytes, offset, 4) != 0) {
        node->nextNode = malloc(sizeof(struct s16_Node));
        node = node->nextNode;
        node->id = readUIntFromBuffer(bytes, offset, 4); offset += 4;
        node->x = readIntFromBuffer(bytes, offset); offset += 4;
        node->y = readIntFromBuffer(bytes, offset); offset += 4;
        node->size = readUIntFromBuffer(bytes, offset, 2); offset += 2;
        node->flags = bytes[offset++];
        node->R = bytes[offset++];
        node->G = bytes[offset++];
        node->B = bytes[offset++];
        if((node->flags & 8) == 8) {
          int slen = strlen((char*) (bytes + offset)) + 1;
          node->name = malloc(sizeof(char) * slen);
          strcpy(node->name, (char*) (bytes + offset));
          offset += slen;
          logs_Updates->info(logs_Updates, "NetHandler", "Node : %d %d %d %d %d %d %d %d %s", node->id, node->x, node->y, node->size, node->flags, node->R, node->G, node->B, node->name);
        } else {
          logs_Updates->info(logs_Updates, "NetHandler", "Node : %d %d %d %d %d %d %d %d", node->id, node->x, node->y, node->size, node->flags, node->R, node->G, node->B);
        }
      }
			node->nextNode = NULL;
    }

    packet.rmSize = readUIntFromBuffer(bytes, offset, 2); offset += 2;
    packet.rmIds = malloc(4 * packet.rmSize);
    for(i = 0; i < packet.rmSize; i++) {
      packet.rmIds[i] = readUIntFromBuffer(bytes, offset, 4);
    }

    handleUpdateNodesPacket(wsi, packet);

		free(packet.deadNodes);
		free(packet.rmIds);

		struct s16_Node* currentNode = packet.nodes;
		struct s16_Node* nextNode;
		if(currentNode != NULL) {

			while(currentNode->nextNode != NULL) {
				nextNode = currentNode->nextNode;
				if((currentNode->flags & 8) == 8) {
					free(currentNode->name);
				}
				free(currentNode);
				currentNode = nextNode;
			}
			free(currentNode);
			logs_Updates->info(logs_Updates, "NetHandler", "Update_Nodes packet freed.");
		}

		break;
	case SPACKET_UPDATE_POSITION:
		logs_Updates->info(logs_Updates, "NetHandler", "Update_Position packet received. (opcode 17)");
		if(length >= 13) {
			struct s_UpdatePosition* packet = (struct s_UpdatePosition*) (bytes+1);
			handleUpdatePositionPacket(wsi, *packet);
		} else {
			// Should never happen as we use TCP but, nevermind.
			logs->err(logs, "NetHandler", "Update_Position packet corrupted, skipping.");
		}
		break;
	default:
		break;
	}
}

float readFloatFromBuffer(unsigned char* buffer, int offset) {
  float result;
  memcpy(&result, buffer + offset, sizeof(float));
  return result;
}

int readIntFromBuffer(unsigned char* buffer, int offset) {
  int result;
  memcpy(&result, buffer + offset, sizeof(int));
  return result;
}

unsigned int readUIntFromBuffer(unsigned char* buffer, int offset, int length) {
  unsigned int result;
  memcpy(&result, buffer + offset, length);
  return result;
}


void sendPacket(struct lws* wsi, ClientPacket* packet) {
	sendCommand(wsi, packet->bytes, packet->length);
}

void handleAddNodePacket(struct lws *wsi, struct s_AddNode packet) {
	logs_Updates->info(logs_Updates, "NetHandler", "Node Id : %d", packet.id);
	int i;
	for(i = 0; i < 16; i++) {
		if(playerCellIds[i] == -1) {
			playerCellIds[i] = packet.id;
			break;
		}
	}
}

void handleClearNodesPacket(struct lws *wsi) {
	// TODO
}

void handleDrawLinePacket(struct lws *wsi, struct s_DrawLine packet) {
	// TODO
}

void handleSetBordersPacket(struct lws *wsi, struct s_SetBorders packet) {
	viewField.left = packet.left;
	viewField.top = packet.top;
	viewField.bottom = packet.bottom;
	viewField.right = packet.right;
}

void handleUpdateNodesPacket(struct lws *wsi, struct s_UpdateNodes packet) {
	int i;
	for(i = 0; i < packet.deSize; i++) {
			int j;
			for(j = 0; j < 16; j++) {
				if(playerCellIds[j] == packet.deadNodes[i].eatenId) {
					playerCellIds[j] = -1;
				}
			}
	}

	clearNodeList(cells);
	clearNodeList(playerCells);
	struct s16_Node* node = packet.nodes;
	Node* copiedNode;
	while(node != NULL) {
		copiedNode = malloc(sizeof(Node));
		copiedNode->id = node->id;
		copiedNode->size = node->size;
		copiedNode->x = node->x;
		copiedNode->y = node->y;
		copiedNode->r = node->R;
		copiedNode->g = node->G;
		copiedNode->b = node->B;
		copiedNode->flags = node->flags;
		copiedNode->next = NULL;
		if((node->flags & 8) == 8) {
			copiedNode->name  = malloc(strlen(node->name) + 1);
			strcpy(copiedNode->name , node->name);
		}
		int j;
		// Check if node is owned by this client
		for(j = 0; j < 16; j++) {
			if(playerCellIds[j] == copiedNode->id) {
				Node* copiedPlayerCell = malloc(sizeof(Node));
				copiedPlayerCell = malloc(sizeof(Node));
				copiedPlayerCell->id = node->id;
				copiedPlayerCell->size = node->size;
				copiedPlayerCell->x = node->x;
				copiedPlayerCell->y = node->y;
				copiedPlayerCell->r = node->R;
				copiedPlayerCell->g = node->G;
				copiedPlayerCell->b = node->B;
				copiedPlayerCell->flags = node->flags;
				copiedPlayerCell->next = NULL;
				if((node->flags & 8) == 8) {
					copiedPlayerCell->name  = malloc(strlen(node->name) + 1);
					strcpy(copiedPlayerCell->name , node->name);
				}
				addNode(playerCells, copiedPlayerCell);
			}
		}
		addNode(cells, copiedNode);
		node = node->nextNode;
	}
}

void handleUpdatePositionPacket(struct lws *wsi, struct s_UpdatePosition packet) {
	// TODO
}
