/**
 * \file packets.c
 * \brief Implémentation du gestionnaire de packets pour le projet Ogar.
 * \author Noé.M
 * \version 0.1
 * \date 30 Juin 2018
 *
 * Implémentation du gestionnaire de packets pour les clients Ogar.
 *
 */


#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#include "packets.h"

// === CLIENT_PACKET (PRIVATE) ===

struct __attribute__((__packed__)) c_connect {
    unsigned char packetId; /* 1 byte */
    unsigned int protocolVersion; /* 4 byte */
};

struct __attribute__((__packed__)) c_target {
    unsigned char packetId; /* 1 byte */
    int x; /* 4 bytes */
    int y; /* 4 bytes */
	int misc; /* 4 bytes */
};

struct __attribute__((__packed__)) c_mono {
    unsigned char packetId; /* 1 byte */
};

// === CLIENT PACKETS BUILDERS ===

// Constructor
ClientPacket * buildPacket(int packetId, ...) {

	int length;
	unsigned char* bytes;

    switch (packetId) {
    case CPACKET_CONNECT:
        {
            // Building connect packet
            struct c_connect * packetBytes = malloc(sizeof(struct c_connect));
            packetBytes->packetId = (unsigned int) packetId;
            packetBytes->protocolVersion = (int) 13;
			length = 5;
			bytes =  (unsigned char * ) packetBytes;
            break;
        }
    case CPACKET_NICKNAME:
        {
            // Getting arguments
            va_list valist;
            va_start(valist, packetId);
            char * name = va_arg(valist, char * );
            va_end(valist);

            // Building nickname packet
			int packetLength = 2 + strlen(name);
            char * packetBytes = malloc(packetLength);
            packetBytes[0] = (char)((unsigned int) packetId);
            strcpy(packetBytes + 1, name);

			length = packetLength;
			bytes = (unsigned char * ) packetBytes;
            break;
        }
    case CPACKET_TARGET:
        {
            // Getting arguments
            va_list valist;
            va_start(valist, packetId);
            int x = va_arg(valist, int);
            int y = va_arg(valist, int);
            va_end(valist);

            // Building target packet
            struct c_target * packetBytes = malloc(sizeof(struct c_target));
            packetBytes->packetId = (unsigned int) packetId;
            packetBytes->x = x;
            packetBytes->y = y;
			packetBytes->misc = 0;
			length = 13;
			bytes =  (unsigned char *) packetBytes;
            break;
        }
    default:
        {
            unsigned char * packetBytes = malloc(sizeof(char));
			*packetBytes = packetId;
			length = 1;
			bytes =  (unsigned char * ) packetBytes;
			break;
        }
    }
	ClientPacket * packet = malloc(sizeof(ClientPacket));
	packet->free = __ClientPacket__free;
	packet->tostr = __ClientPacket__tostr;
	packet->packetId = packetId;
	packet->bytes = bytes;
	packet->length = length;
	return packet;
}

// Packet Destructor
void __ClientPacket__free(ClientPacket* packet) {
	free(packet->bytes);
	free(packet);
}

// Packet Formatter
char* __ClientPacket__tostr(ClientPacket* packet) {
	char strId[12];
	char strLength[12];
	sprintf(strId, "%d", packet->packetId);
	sprintf(strLength, "%d", packet->length);

	char* str = malloc(5 * packet->length + 56);
	strcpy(str, "[ id = ");
	strcat(str, strId);
	strcat(str, ", length = ");
	strcat(str, strLength);
	strcat(str, ", bytes = ");
	int i;
	for(i = 0; i < packet->length; i++) {
		char strByte[5];
		sprintf(strByte, "0x%02x ", (int) (packet->bytes[i]));
		strcat(str, strByte);
	}
	strcat(str, "]");
	return str;
}
