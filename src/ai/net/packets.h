/**
 * \file packets.h
 * \brief Gestionnaire de packets pour le projet Ogar.
 * \author Noé.M
 * \version 0.1
 * \date 30 Juin 2018
 *
 * Gestionnaire de packets pour les clients Ogar.
 *
 */

// PACKET IDENTIFIERS

// CLIENT PACKETS
#define CPACKET_CONNECT 254
#define CPACKET_NICKNAME 0
#define CPACKET_SPECTATE 1
#define CPACKET_TARGET 16
#define CPACKET_SPLIT 17
#define CPACKET_QPRESSED 18
#define CPACKET_QRELEASED 19
#define CPACKET_EJECTMASS 21

#define SPACKET_ADD_NODE 32
#define SPACKET_CLEAR_NODES 18
#define SPACKET_DRAW_LINE 21
#define SPACKET_SET_BORDERS 64
#define SPACKET_UPDATE_NODES 16
#define SPACKET_UPDATE_POSITION 17

/**
 * \struct ClientPacket
 * \brief Objet packet côté client.
 *
 * PacketClient est un objet contenant toutes les valeurs nécessaires pour gérer et envoyer les paquets clients.
 *
 */
typedef struct ClientPacket {
	int packetId; // Identifiant du paquet
	int length; // Taille du paquet en bytes
	unsigned char* bytes; 
	void (*free)(struct ClientPacket* packet);
	char* (*tostr)(struct ClientPacket* packet);
} ClientPacket;


/**
 * \fn ClientPacket* buildPacket(int packetId, ...)
 * \brief Construit un packet binaire à partir des éléments donnés.
 *
 * \param packetId Identifiant du paquet, voir la liste des paquets.
 * \return ClientPacket à envoyer sur le réseau.
 */
ClientPacket* buildPacket(int packetId, ...);

/**
 * \fn void __ClientPacket__free(ClientPacket* packet)
 * \brief Destructeur de ClientPacket et libère l'espace mémoire.
 *
 * \param ClientPacket le paquet à détruire.
 */
void __ClientPacket__free(ClientPacket* packet);

/**
 * \fn void __ClientPacket__print(ClientPacket* packet)
 * \brief Formatte et écrit le paquet sur le flux pour les tests unitaires.
 *
 * \param ClientPacket.
 */
char* __ClientPacket__tostr(ClientPacket* packet);
