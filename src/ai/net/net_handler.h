/**
 * \file net_handler.h
 * \brief Module de gestion du réseau.
 * \author Noé.M
 * \version 0.2
 * \date 03 Juin 2018
 *
 * Module de gestion du réseau.
 *
 */

//*************************************************************

// Structures locales, représentation mémoire des paquets serveur.

/**
 * \struct s_AddNode
 * \brief Représentation mémoire du paquet Add Node.
 */
struct __attribute__((__packed__)) s_AddNode {
    unsigned int id; /* 4 bytes */
};

/**
 * \struct s_DrawLine
 * \brief Représentation mémoire du paquet Draw Line.
 */
struct __attribute__((__packed__)) s_DrawLine {
    short unsigned int x; /* 2 bytes */
    short unsigned int y; /* 2 bytes */
};

/**
 * \struct s_SetBorders
 * \brief Représentation mémoire du paquet Set Borders.
 */
struct __attribute__((__packed__)) s_SetBorders {
    double left; /* 8 bytes */
    double top; /* 8 bytes */
    double right; /* 8 bytes */
    double bottom; /* 8 bytes */
};

/**
 * \struct s_UpdateNodes
 * \brief Représentation mémoire du paquet Update Nodes.
 */
struct __attribute__((__packed__)) s_UpdateNodes {
    short unsigned int deSize; /* 2 bytes */
    struct s16_DeadNodes* deadNodes;
    struct s16_Node* nodes;
    short unsigned int rmSize;
    unsigned int* rmIds;
};

/**
 * \struct s_UpdatePosition
 * \brief Représentation mémoire du paquet Update Position.
 */
struct __attribute__((__packed__)) s_UpdatePosition {
  float x; /* 4 bytes */
  float y; /* 4 bytes */
  float size; /* 4 bytes */
};

// Specific for Update Nodes packet

/**
 * \struct s16_DeadNodes
 * \brief Représentation mémoire d'un noeud mort pour le paquet Update Nodes.
 */
struct __attribute__((__packed__)) s16_DeadNodes {
    unsigned int eatenId; /* 4 bytes */
    unsigned int eaterId; /* 4 bytes */
};

/**
 * \struct s16_Node
 * \brief Représentation mémoire d'un noeud pour le paquet Update Nodes.
 */
struct __attribute__((__packed__)) s16_Node {
    unsigned int id; /* 4 bytes */
    int x; /* 4 bytes */
    int y; /* 4 bytes */
    short unsigned int size; /* 2 bytes */
    unsigned char R;  /* 1 byte */
    unsigned char G;  /* 1 byte */
    unsigned char B;  /* 1 byte */
    unsigned char flags;  /* 1 byte */
    char* name;
    struct s16_Node* nextNode;
};

//*************************************************************

/**
 * \fn void handleMessage (struct lws *wsi, unsigned char* bytes, int length)
 * \brief Fonction appelée lors de la reception d'un paquet.
 *
 * \param struct lws Contexte.
 * \param unsigned char* bytes Données du paquet reçu.
 * \param int length Taille des données à envoyer.
 */
void handleMessage(struct lws *wsi, unsigned char* bytes, int length);


/**
 * \fn void sendPacket (struct lws *wsi, ClientPacket* packet)
 * \brief Raccourci de la commande sendCommand prenant un paquet en paramètre.
 *
 * \param struct lws Contexte.
 * \param ClientPacket.
 */
void sendPacket(struct lws* wsi, ClientPacket* packet);

/**
 * \fn float readFloatFromBuffer(unsigned char* buffer, int offset)
 * \brief Lit un flottant dans un buffer (unsigned char*) et le renvoie.
 *
 * \param unsigned char* buffer Buffer dans lequel effectuer la lecture.
 * \param int offset Indice de début de la lecture.
 * 
 * \return float Flottant lu.
 */
float readFloatFromBuffer(unsigned char* buffer, int offset);

/**
 * \fn float readIntFromBuffer(unsigned char* buffer, int offset)
 * \brief Lit un entier signé dans un buffer (unsigned char*) et le renvoie.
 *
 * \param unsigned char* buffer Buffer dans lequel effectuer la lecture.
 * \param int offset Indice de début de la lecture.
 * 
 * \return float Entier signé lu.
 */
int readIntFromBuffer(unsigned char* buffer, int offset);

/**
 * \fn float readUIntFromBuffer(unsigned char* buffer, int offset, int length)
 * \brief Lit un entier non signé dans un buffer (unsigned char*) et le renvoie.
 *
 * \param unsigned char* buffer Buffer dans lequel effectuer la lecture.
 * \param int offset Indice de début de la lecture.
 * \param int length Taille en octets de l'entier.
 * 
 * \return float Entier non signé lu.
 */
unsigned int readUIntFromBuffer(unsigned char* buffer, int offset, int length);

/**
 * \fn void handleAddNodePacket(struct lws *wsi, struct s_AddNode packet)
 * \brief Fonction appelée une fois le paquet Add Nodes lu. Sert à organiser les données dans le jeu.
 *
 * \param struct lws *wsi Contexte libwebsockets.
 * \param struct s_AddNode packet Paquet Add Node.
 */
void handleAddNodePacket(struct lws *wsi, struct s_AddNode packet);

/**
 * \fn void handleClearNodesPacket(struct lws *wsi)
 * \brief Fonction appelée une fois le paquet Clear Nodes lu. Sert à organiser les données dans le jeu.
 *
 * \param struct lws *wsi Contexte libwebsockets.
 */
void handleClearNodesPacket(struct lws *wsi);

/**
 * \fn void handleDrawLinePacket(struct lws *wsi, struct s_DrawLine packet)
 * \brief Fonction appelée une fois le paquet Draw Line lu. Sert à organiser les données dans le jeu.
 *
 * \param struct lws *wsi Contexte libwebsockets.
 * \param struct s_DrawLine packet Paquet Draw Line.
 */
void handleDrawLinePacket(struct lws *wsi, struct s_DrawLine packet);

/**
 * \fn void handleSetBordersPacket(struct lws *wsi, struct s_SetBorders packet)
 * \brief Fonction appelée une fois le paquet Set Borders lu. Sert à organiser les données dans le jeu.
 *
 * \param struct lws *wsi Contexte libwebsockets.
 * \param struct s_SetBorders packet Paquet Set Borders.
 */
void handleSetBordersPacket(struct lws *wsi, struct s_SetBorders packet);

/**
 * \fn void handleUpdateNodesPacket(struct lws *wsi, struct s_UpdateNodes packet)
 * \brief Fonction appelée une fois le paquet Update Nodes lu. Sert à organiser les données dans le jeu.
 *
 * \param struct lws *wsi Contexte libwebsockets.
 * \param struct s_UpdateNodes packet Paquet Update Nodes.
 */
void handleUpdateNodesPacket(struct lws *wsi, struct s_UpdateNodes packet);

/**
 * \fn void handleUpdatePositionPacket(struct lws *wsi, struct s_UpdatePosition packet)
 * \brief Fonction appelée une fois le paquet Update Position lu. Sert à organiser les données dans le jeu.
 *
 * \param struct lws *wsi Contexte libwebsockets.
 * \param struct s_UpdatePosition packet Paquet Update Position.
 */
void handleUpdatePositionPacket(struct lws *wsi, struct s_UpdatePosition packet);
