#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <unistd.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <SDL2/SDL_ttf.h>

#include "../../lib/logger/logger.c"

#include "launcher.h"

#include "net/client.c"

#define OPT_STACKTRACE 1000
#define OPT_PKTDATA 1001
#define OPT_USERNAME 1002
#define OPT_CONTROLLER 1003
#define OPT_DISPLAY 1004
#define OPT_DEBUG 1006
#define OPT_DISPLAY_FULLSCREEN 1005

char* username;
int controllerId;

int display;
int debug;

// Display config (display.ini)
int screenWidth;
int screenHeight;
int backgroundColorR;
int backgroundColorG;
int backgroundColorB;

int borderColorR;
int borderColorG;
int borderColorB;

Uint32 fullscreen;

Logger* logs;
Logger* logs_Updates;

SDL_Renderer* renderer = NULL;
TTF_Font* pFont;



struct option longopts[] = {
   { "stacktrace", no_argument, NULL, OPT_STACKTRACE},
   { "packetData",    no_argument, NULL, OPT_PKTDATA},
   { "username",    required_argument, NULL, OPT_USERNAME},
   { "controller", required_argument, NULL, OPT_CONTROLLER},
   { "display"   , no_argument, NULL, OPT_DISPLAY},
   { "display-fullscreen"   , no_argument, NULL, OPT_DISPLAY_FULLSCREEN},
   { "debug"   , no_argument, NULL, OPT_DEBUG},
   { NULL, 0, NULL, 0 }
};

int main(int argc, char** argv) {

	screenWidth = 1024;
	screenHeight = 592;
	backgroundColorR = 250;
	backgroundColorG = 250;
	backgroundColorB = 250;

	borderColorR = 0;
	borderColorG = 0;
	borderColorB = 0;

	display = 0;
  debug = 0;
	logs = buildLogger("Ogar-AI");
	logs->enabled = 0;

	fullscreen = 0;

	controllerId = 0;

  cells = malloc(sizeof(Node**));
  *cells = NULL;
  playerCells = malloc(sizeof(Node**));
  *playerCells = NULL;
  int j;
  for(j = 0; j < 16; j++) {
    playerCellIds[j] = -1;
  }

	logs_Updates = buildLogger("Ogar-AI");
	logs_Updates->enabled = 0;

	int n = 0;

	struct lws_context_creation_info info;
	struct lws_client_connect_info i;

	const char *protocol,*temp;

	memset(&info, 0, sizeof info);
	memset(&i, 0, sizeof(i));

	if (argc < 2)
		goto usage;

	while (n >= 0) {
		int option_index = 0;
		n = getopt_long(argc, argv, "hsp:P:o:", longopts, &option_index);
		if (n < 0)
			continue;
		switch (n) {
		case OPT_STACKTRACE:
			logs->enabled = 1;
			logs->info(logs, "Debug", "Trace enabled.");
			break;
		case OPT_PKTDATA:
			logs_Updates->enabled = 1;
			logs->enabled = 1;
			logs->info(logs, "Debug", "Packet data logging activated.");
			break;
		case OPT_USERNAME:
			username = optarg;
			break;
  	case OPT_DISPLAY:
  			display = 1;
  		break;
    case OPT_DEBUG:
  			debug = 1;
  		break;
		case OPT_DISPLAY_FULLSCREEN:
			fullscreen = SDL_WINDOW_FULLSCREEN;
			display = 1;
			break;
		case OPT_CONTROLLER:
			if(!strcasecmp(optarg, "manual")) {
				logs->info(logs, "Controller", "Controller set to Manual.");
				controllerId = 0;
				display = 1;
			} else if(!strcasecmp(optarg, "ai-lazy")) {
				logs->info(logs, "Controller", "Controller set to AI-Lazy.");
				controllerId = 1;
			} else if(!strcasecmp(optarg, "ai-ray")) {
				logs->info(logs, "Controller", "Controller set to AI-Ray.");
				controllerId = 2;
      } else if(!strcasecmp(optarg, "ai-gradient")) {
				logs->info(logs, "Controller", "Controller set to AI-Gradient.");
				controllerId = 3;
      } else if(!strcasecmp(optarg, "ai-gravity")) {
				logs->info(logs, "Controller", "Controller set to AI-Gravity.");
				controllerId = 4;
      } else if(!strcasecmp(optarg, "ai-polar")) {
				logs->info(logs, "Controller", "Controller set to AI-Polar.");
				controllerId = 5;
      } else {
				controllerId = 0;
				logs->warn(logs, "Controller", "This controller does not exist. \n Current controllers : \n - manual \n - ai-lazy");
				logs->warn(logs, "Controller", "Using manual controller as default.");
				display = 1;
			}
			break;
		case 's':
			i.ssl_connection = 2;
			break;
		case 'p':
			i.port = atoi(optarg);
			break;
		case 'o':
			i.origin = optarg;
			break;
		case 'P':
			info.http_proxy_address = optarg;
			break;
		case 'h':
			goto usage;
		}
	}


	readDisplayConfig();

	if(username == NULL) {
		logs->warn(logs, "Client", "No username specified, use option -u to specify one. Using \"Pingu\" as default.");
		username = "Pingu";
	}

	if (optind >= argc)
		goto usage;


	if(controllerId == 0) {
		display = 1;
	}

	if (lws_parse_uri(argv[optind], &protocol, &i.address, &i.port, &temp))
		goto usage;

  if(display) {
    SDL_Init(SDL_INIT_VIDEO);
    /* Création de la fenêtre */
    SDL_Window* pWindow = NULL;
    pWindow = SDL_CreateWindow("Ogar-Client",SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, screenWidth, screenHeight, SDL_WINDOW_SHOWN | fullscreen);
    renderer =  SDL_CreateRenderer(pWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	if(TTF_Init() == -1)
		return 0;

	pFont = TTF_OpenFont("fonts/Roboto-Regular.ttf", 60);
    // RENDU
    SDL_RenderClear(renderer);
    rectangleRGBA(renderer, 0, 0, screenWidth, screenHeight, backgroundColorR, backgroundColorG, backgroundColorB, 255);
    // filledCircleRGBA(renderer, 100, 100, 10, 255, 0, 0, 255);
    SDL_RenderPresent(renderer);

    startClient(info, i, protocol);
    SDL_DestroyWindow(pWindow);
    SDL_Quit();
 } else {
  startClient(info, i, protocol);
 }

	return 0;


usage:
	fprintf(stderr, "Usage: ogar-client -h -s -p <port> -P <proxy> -o <origin>  <server address> \n");
	return 1;

}


void readDisplayConfig() {
    FILE * fp;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;

    fp = fopen("ogar-client.ini", "r");
    if (fp == NULL) {
		logs->err(logs, "Config", "Couldn't read ogar-client.ini, file is corrupted or doesn't exists.");
		return;
	}

    while ((read = getline(&line, &len, fp)) != -1) {
        char property[50], value[50];
		if(sscanf(line, "%20[^=]=%s ", property, value) != EOF) {
			logs->info(logs, "Config", "Property %s is set to %s", property, value);
			if(strcasecmp(property, "width") == 0) {
				screenWidth = atoi(value);
			} else if(strcasecmp(property, "height") == 0) {
				screenHeight = atoi(value);
			} else if(strcasecmp(property, "backgroundColor") == 0) {
				sscanf(value, "(%d,%d,%d)", &backgroundColorR, &backgroundColorG, &backgroundColorB);
			} else if(strcasecmp(property, "borderColor") == 0) {
				sscanf(value, "(%d,%d,%d)", &borderColorR, &borderColorG, &borderColorB);
			} else if(strcasecmp(property, "fullscreen") == 0) {
				if(strcasecmp(value, "on") == 0) {
					fullscreen = SDL_WINDOW_FULLSCREEN;
				}
			}
		}
    }

    fclose(fp);
    if (line) free(line);
}
