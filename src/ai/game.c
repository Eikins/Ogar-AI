#include <stdlib.h>
#include <math.h>
#include "game.h"
#include "controller/controller.c"

#define min(a,b) (a<=b?a:b)
// compile with gcc -Wall -g -o sock ./game.c -lwebsockets
// call with: ogar-client -o agar.io 127.0.0.1:1443

Node** cells;
ViewField viewField;

Node** playerCells;
int playerCellIds[16];

// Coordonnées du centre de l'écran
int centerPosX;
int centerPosY;

// Taille de l'écran
int sightRangeX;
int sightRangeY;

long int tickElapsed = 0;

void update(struct lws *wsi) {

	if(controller == NULL) {
		controller = buildController(controllerId);
	}

	calcCenterAndSight();

	if(display) renderDisplay();

	if(playerCells != NULL) {
		controller->update(controller);
	}

	ClientPacket* movePacket = buildPacket(CPACKET_TARGET, controller->targetX, controller->targetY);
	sendPacket(wsi, movePacket);
	logs_Updates->info(logs_Updates, "NetHandler", "Sending target packet.");
	logs_Updates->info(logs_Updates, "NetHandler", movePacket->tostr(movePacket));
	movePacket->free(movePacket);

	if(controller->split) {
		ClientPacket* splitPacket = buildPacket(CPACKET_SPLIT);
		sendPacket(wsi, splitPacket);
		logs_Updates->info(logs_Updates, "NetHandler", "Sending split packet.");
		logs_Updates->info(logs_Updates, "NetHandler", splitPacket->tostr(splitPacket));
		splitPacket->free(splitPacket);
		controller->split = 0;
	}

	if(controller->ejectMass) {
		ClientPacket* ejectMassPacket = buildPacket(CPACKET_EJECTMASS);
		sendPacket(wsi, ejectMassPacket);
		logs_Updates->info(logs_Updates, "NetHandler", "Sending eject mass packet.");
		logs_Updates->info(logs_Updates, "NetHandler", ejectMassPacket->tostr(ejectMassPacket));
		ejectMassPacket->free(ejectMassPacket);
		controller->ejectMass = 0;
	}

	tickElapsed++;

}

void renderDisplay() {
	SDL_SetRenderDrawColor(renderer, backgroundColorR, backgroundColorG, backgroundColorB, 255);
	SDL_RenderClear(renderer);
	int borderScreenLeft = (viewField.left - centerPosX + sightRangeX / 2) * screenHeight / sightRangeY;
	int borderScreenRight = (viewField.right - centerPosX + sightRangeX / 2) * screenHeight / sightRangeY;
	int borderScreenTop = (viewField.top - centerPosY + sightRangeY / 2) * screenHeight / sightRangeY;
	int borderScreenBottom = (viewField.bottom - centerPosY + sightRangeY / 2) * screenHeight / sightRangeY;
	rectangleRGBA(renderer, borderScreenLeft, borderScreenTop, borderScreenRight, borderScreenBottom, borderColorR, borderColorG, borderColorB, 255);
	if(playerCells != NULL) {
		Node* cell = *cells;
		while(cell != NULL) {
			filledCircleRGBA(renderer, (cell->x - centerPosX + sightRangeX / 2) * screenWidth / sightRangeX, (cell->y - centerPosY + sightRangeY / 2) * screenHeight / sightRangeY, cell->size * screenHeight / sightRangeY, cell->r, cell->g, cell->b, 255);
			if((cell->flags & 8) == 8)
			{
				int nameLength = strlen(cell->name);
				char* toWrite = malloc(nameLength + 1);
				strcpy(toWrite, cell->name);
				SDL_Color color;
				color.r = borderColorR;
				color.g = borderColorG;
				color.b = borderColorB;
				color.a = 255;
				SDL_Surface* textSurface = TTF_RenderUTF8_Blended(pFont, toWrite, color);
				SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, textSurface);
				SDL_Rect rect;
				rect.x = (cell->x - centerPosX + sightRangeX / 2 - nameLength * cell->size / 4) * screenWidth / sightRangeX;
				rect.y = (cell->y - centerPosY + sightRangeY / 2) * screenWidth / sightRangeX;
				rect.w = cell->size * nameLength / 2 * screenWidth / sightRangeX;
				rect.h = cell->size * screenWidth / sightRangeX;

				SDL_RenderCopy(renderer, texture, NULL, &rect);

				SDL_FreeSurface(textSurface);
				SDL_DestroyTexture(texture);
			}
			cell = cell->next;
		}
	}
	if(debug && playerCells != NULL && *playerCells != NULL) {
		filledCircleRGBA(renderer, (controller->targetX - centerPosX + sightRangeX / 2) * screenWidth / sightRangeX, (controller->targetY - centerPosY + sightRangeY / 2) * screenHeight / sightRangeY, 4 * screenHeight / sightRangeY, 0, 0, 0, 255);
		filledCircleRGBA(renderer, (getNode(playerCells, biggestplayercell(playerCells))->x - centerPosX + sightRangeX / 2) * screenWidth / sightRangeX, (getNode(playerCells, biggestplayercell(playerCells))->y - centerPosY + sightRangeY / 2) * screenHeight / sightRangeY, 4 * screenHeight / sightRangeY, 0, 0, 0, 255);
		circleRGBA(renderer, (getNode(playerCells, biggestplayercell(playerCells))->x - centerPosX + sightRangeX / 2) * screenWidth / sightRangeX, (getNode(playerCells, biggestplayercell(playerCells))->y - centerPosY + sightRangeY / 2) * screenHeight / sightRangeY, approximatedsize(playerCells) * screenHeight / sightRangeY, 0, 0, 0, 255);
		circleRGBA(renderer, (controller->targetX - centerPosX + sightRangeX / 2) * screenWidth / sightRangeX, (controller->targetY - centerPosY + sightRangeY / 2) * screenHeight / sightRangeY, approximatedsize(playerCells) * screenHeight / sightRangeY, 240, 0, 0, 255);
	}
	SDL_RenderPresent(renderer);
}

void calcCenterAndSight() {
	int i;
	centerPosX = 0;
	centerPosY = 0;
	int len = 0;
	int totalSize = 0;
	for(i = 0; i < 16; i++) {
		if (playerCellIds[i] != -1) {
			len++;
			Node* node = getNode(playerCells, playerCellIds[i]);
			if(node != NULL) {
				centerPosX += node->x;
				centerPosY += node->y;
				totalSize += node->size;
			}
		}
	}
	if(len != 0) {
		centerPosX /= len;
		centerPosY /= len;
	}

	// Calc sight
	if(totalSize != 0) {
			double factor = pow(min(1, 64.0 / totalSize), 0.4);
			sightRangeX = screenWidth / factor;
			sightRangeY = screenHeight / factor;
	}
}

void addNode(Node** list, Node* element) {
	element->next = *list;
	*list = element;
}

void removeNode(Node** list, unsigned int id) {
	if(list == NULL || *list == NULL) {
		return;
	}

	if((*list)->next == NULL) {
		if((*list)->id == id) {
			free(*list);
			*list = NULL;
		}
		return;
	}

	Node* node = *list;
	Node* lastNode = NULL;
	while(node != NULL) {
		if(node->id == id) {
			if(lastNode == NULL) {
				*list = (*list)->next;
				free(node);
				return;
			} else {
				lastNode->next = node->next;
				free(node);
				return;
			}
		}
		lastNode = node;
		node = node->next;
	}
}

Node* getNode(Node**list, unsigned int id) {
	if(list == NULL) {
		return NULL;
	}
	Node* ret = *list;
	while(ret != NULL) {
		if(ret->id == id) {
			return ret;
		}
		ret = ret->next;
	}
	return ret;
}

void clearNodeList(Node** list) {
	Node* currentNode = *list;
	Node* nextNode;
	if(currentNode != NULL) {
		while(currentNode->next != NULL) {
			nextNode = currentNode->next;
			if((currentNode->flags & 8) == 8) {
				free(currentNode->name);
			}
			free(currentNode);
			currentNode = nextNode;
		}
		free(currentNode);
	}
	*list = NULL;
}

void foreachNode(Node** list, void (*func)(Node*)) {
	Node* node = *list;
	while(node != NULL) {
		func(node);
		node = node->next;
	}
}
