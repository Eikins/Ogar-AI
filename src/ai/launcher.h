extern char* username;
extern int display;
extern int debug;
extern int controllerId;

extern int screenWidth;
extern int screenHeight;
extern int backgroundColorR;
extern int backgroundColorG;
extern int backgroundColorB;

extern int borderColorR;
extern int borderColorG;
extern int borderColorB;

extern SDL_Renderer* renderer;

extern Logger* logs;
extern Logger* logs_Updates;

extern TTF_Font* pFont;

void readDisplayConfig();
