#include <stdio.h>
#include <stdlib.h>
#include <math.h>


typedef struct node {

  unsigned int id; /* 4 bytes */
  int x; /* 4 bytes */
  int y; /* 4 bytes */
  short unsigned int size; /* 2 bytes */
  unsigned char R;  /* 1 byte */
  unsigned char G;  /* 1 byte */
  unsigned char B;  /* 1 byte */
  unsigned char flags;  /* 1 byte */
  char* name;
  struct node* next;


}Node;


void add_node_to_list(Node ** liststart, Node * newnode){

  Node *p;
  p = *liststart;

if(p==NULL){


*liststart = newnode;



} else {

  while(p->next != NULL){
    p = p->next;
  }

 p->next= newnode;


      }


  }


void viewlist(Node * liststart){

  Node *p;
  p=liststart;

  while(p != NULL)
  {
    printf("id=%d\nx=%d\ny=%d\nsize=%d\nR=%d\nG=%d\nB=%d\nflags=%d\nname=%s\n\n\n",p->id,p->x,p->y,p->size,p->R,p->G,p->B,p->flags,p->name);
    p=p->next;

  }


}



void main(void){

Node *foods;
foods = NULL;


Node *p;
p = malloc(sizeof(Node));
p->id = 1;
p->x = 2;
p->y = 1;
p->size = 5;
p->R = 5;
p->G = 6;
p->B = 7;
p->flags = 2;
p->name = "ooo";
p->next = NULL;


Node *p0;
p0 = malloc(sizeof(Node));
p0->id = 2;
p0->x = 0;
p0->y = 0;
p0->size = 5;
p0->R = 0;
p0->G = 6;
p0->B = 0;
p0->flags = 2;
p0->name = "ooo";
p0->next = NULL;

Node *p1;
p1 = malloc(sizeof(Node));
p1->id = 15;
p1->x = 100;
p1->y = 100;
p1->size = 10;
p1->R = 0;
p1->G = 6;
p1->B = 0;
p1->flags = 2;
p1->name = "ooo";
p1->next = NULL;

add_node_to_list(&foods,p);
add_node_to_list(&foods,p0);
add_node_to_list(&foods,p1);
/*viewlist(foods);
removeNode(&foods, 14);
viewlist(foods);
removeNode(&foods, 1);
*/
//viewlist(foods);

double a = 5.516161131;
double b = 5.431257432;
 //printf("%d\n",distance(p0,p1));



printf("%d\n",biggestplayercell(&foods));

__ControllerRay__update(foods);

}



void __ControllerRay__update(Node *cells) {


  Node* node = cells;
  Node* nodecmp = cells;

  int bestcirclegain = 0;
  int bestX =0;
  int bestY =0;
  int currentcirclegain = 0;

  while(node != NULL && (node->flags & 8) != 8 && (node->flags & 1) != 1) { // for all food cells


      while(nodecmp != NULL){

        if(distance(node,nodecmp) < biggestplayercell(&cells)){
          currentcirclegain += nodecmp->size;
        }
        nodecmp=nodecmp->next;
      }


      printf("current : %d\n",currentcirclegain);

      if(currentcirclegain > bestcirclegain){
        bestcirclegain = currentcirclegain;
        bestX = node->x;
        bestY = node->y;
      }

      currentcirclegain =0;

      node= node->next;
      nodecmp = cells;
      }


        printf("best : %d\n",bestcirclegain);
        printf("bestX : %d\n",bestX);
        printf("bestY : %d\n",bestY);

    }







int distance(Node *cell1,Node *cell2){

 return sqrt( pow(cell1->x - cell2->x,2) + pow(cell1->y - cell2->y,2));


}




int biggestplayercell(Node ** playerCells){


Node * node = *playerCells;

int big = -1;

while(node != NULL) {

if(node->size > big){
  big = node->size;
}
node = node->next;
}

return big;

}





void removeNode(Node** list, unsigned int id) {
	if(list == NULL || *list == NULL) {
		return;
	}

	if((*list)->next == NULL) {
		if((*list)->id == id) {
			free(*list);
			*list = NULL;
		}
		return;
	}

	Node* element = *list;
	Node* lastElement = NULL;
	while(element != NULL) {
		if(element->id == id) {
			if(lastElement == NULL) {
				*list = (*list)->next;
				free(element);
				return;
			} else {
				lastElement->next = element->next;
				free(element);
				return;
			}
		}
    lastElement = element;
    element = element->next;
	}
}
