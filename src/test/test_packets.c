#include <stdio.h>
#include "../ai/net/packets.c"
#include "../../lib/logger/logger.c"
// PROTOTYPES
float readFloatFromBuffer(unsigned char* buffer, int offset);

int main() {

	Logger* logger = buildLogger("Tests Unitaires");
	logger->info(logger, "Packets", "Begining unit tests...");

	logger->info(logger, "Packets", "Packet Connect :");
	ClientPacket * connectPacket = buildPacket(CPACKET_CONNECT);
	logger->info(logger, "Packets", connectPacket->tostr(connectPacket));
	connectPacket->free(connectPacket);

	logger->info(logger, "Packets", "Packet Nickname :");
	ClientPacket * nicknamePacket = buildPacket(CPACKET_NICKNAME, "Pingunator");
	logger->info(logger, "Packets", nicknamePacket->tostr(connectPacket));
	connectPacket->free(nicknamePacket);

	logger->warn(logger, "Packets", "Packet are encoded in little endian.");

	printf("\n\n");


}
