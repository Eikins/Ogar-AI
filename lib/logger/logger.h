/**
 * \file logger.h
 * \brief Module de gestion des logs.
 * \author Noé.M
 * \version 0.1
 * \date 01 Juin 2018
 *
 * Module de gestion des logs.
 *
 */

/**
 * \struct Logger
 * \brief Objet Logger.
 *
 * Permet d'écrire des informations de type info, erreur et warning si activé.
 *
 */
typedef struct Logger {
	char* name;
	int enabled;
	void (*err)(struct Logger*, char* category, char* message, ...);
	void (*warn)(struct Logger*, char* category, char* message, ...);
	void (*info)(struct Logger*, char* category, char* message, ...);
} Logger;

/**
 * \fn Logger* buildLogger(char* name)
 * \brief Constructeur de l'objet Logger.
 *
 * \param char* name Nom du Logger.
 * 
 * \return Logger Logger construit.
 */
Logger* buildLogger(char* name);

/**
 * \fn void __Logger__err(Logger* log, char* category, char* message, ...)
 * \brief Ecrit un message d'erreur.
 *
 * \param Logger* log Logger.
 * \param char* category Nom du module utilisateur.
 * \param char* message Message (format) à écrire.
 */
void __Logger__err(Logger* log, char* category, char* message, ...);

/**
 * \fn void __Logger__warn(Logger* log, char* category, char* message, ...)
 * \brief Ecrit un message d'alerte.
 *
 * \param Logger* log Logger.
 * \param char* category Nom du module utilisateur.
 * \param char* message Message (format) à écrire.
 */
void __Logger__warn(Logger* log, char* category, char* message, ...);

/**
 * \fn void __Logger__info(Logger* log, char* category, char* message, ...)
 * \brief Ecrit un message d'information.
 *
 * \param Logger* log Logger.
 * \param char* category Nom du module utilisateur.
 * \param char* message Message (format) à écrire.
 */
void __Logger__info(Logger* log, char* category, char* message, ...);
