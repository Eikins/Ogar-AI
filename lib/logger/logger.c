#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include "logger.h"


Logger* buildLogger(char* name) {
	Logger* log = malloc(sizeof(Logger));
	log->name = name;
	log->enabled = 1;
	log->err = __Logger__err;
	log->warn = __Logger__warn;
	log->info = __Logger__info;
	return log;
}

void __Logger__err(Logger* log, char* category, char* message, ...) {
	if(log->enabled) {
		printf("\033[0;31m");
		printf("[ERROR] [%s] (%s) ", log->name, category);
		va_list args;
		va_start(args, message);
		vprintf(message, args);
		va_end(args);
		printf("\033[0m");
		printf("\n");
	}
}

void __Logger__warn(Logger* log, char* category, char* message, ...) {
	if(log->enabled) {
		printf("\033[0;33m");
		printf("[WARNING] [%s] (%s) ", log->name, category);
		va_list args;
		va_start(args, message);
		vprintf(message, args);
		va_end(args);
		printf("\033[0m");
		printf("\n");
	}
}

void __Logger__info(Logger* log, char* category, char* message, ...) {
	if(log->enabled) {
		printf("[INFO] [%s] (%s) ", log->name, category);
		va_list args;
		va_start(args, message);
		vprintf(message, args);
		va_end(args);
		printf("\n");
	}
}
