# Compiler Ogar-AI

```Shell
make ogar-client
```

# Lancer Ogar-AI

```Shell
./ogar-client -o agar.io 127.0.0.1:1443 --stacktrace --packetData --username=Ogar-AI --controller=manual --display-fullscreen
```

**Options disponibles**

 * **--stacktrace** Affiche la trace du programme
 * **--packetData** Affiche tous les échanges de paquets ainsi que leurs données décodées.
 * **--username** Choisi un pseudonyme. (Pingu par défaut)
 * **--controller** Choisi un controller :
	 * **manual** (Par défaut) Contrôle au clavier/souris.
	 * **ai-lazy** Utilise l'IA "Lazy" : mange ce qu'il y a de plus proche.
	 * **ai-ray** Utilise l'IA "Ray" : se déplace vers les zones les plus denses basées sur son rayon.
 * **--display** Lance un affichage SDL pour voir le jeu. (Obligatoire lors de l'utilisation du controller manual) 
 * **--display-fullscreen** Lance un affichage en plein écran.