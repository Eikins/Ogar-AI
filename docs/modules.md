# Modules

## PacketManager

### Packet List

#### Client Packets

```
CPACKET_CONNECT
CPACKET_NICKNAME
CPACKET_SPECTATE
CPACKET_TARGET
CPACKET_SPLIT
CPACKET_QPRESSED
CPACKET_QRELEASED
CPACKET_EJECTMASS
```

#### Server Packets

```
```

### Building a packet 

```C
unsigned char* buildPacket(int packetId, ...)
```