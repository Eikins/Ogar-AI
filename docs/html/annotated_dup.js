var annotated_dup =
[
    [ "ClientPacket", "struct_client_packet.html", "struct_client_packet" ],
    [ "Controller", "struct_controller.html", "struct_controller" ],
    [ "Logger", "struct_logger.html", "struct_logger" ],
    [ "Node", "struct_node.html", "struct_node" ],
    [ "s16_DeadNodes", "structs16___dead_nodes.html", null ],
    [ "s16_Node", "structs16___node.html", null ],
    [ "s_AddNode", "structs___add_node.html", null ],
    [ "s_DrawLine", "structs___draw_line.html", null ],
    [ "s_packet", "structs__packet.html", "structs__packet" ],
    [ "s_SetBorders", "structs___set_borders.html", null ],
    [ "s_UpdateNodes", "structs___update_nodes.html", null ],
    [ "s_UpdatePosition", "structs___update_position.html", null ],
    [ "ViewField", "struct_view_field.html", "struct_view_field" ]
];