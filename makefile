FLAGS = -Wall

ogar-client : src/ai/launcher.c
	gcc $(FLAGS) -o $@ $^ -g -lwebsockets -lSDL2 -lSDL2main -lSDL2_gfx -lSDL2_ttf -lm

test_packets : src/test/test_packets.c
	gcc $(FLAGS) -o $@ $^
